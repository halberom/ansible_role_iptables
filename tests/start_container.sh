#!/bin/bash
set -x

# check for distribution and tag, provide defaults
[ "$distribution" ] || distribution="centos"
[ "$tag" ] || tag="7.2.1511"

# the name the continer will be run as, e.g. centos721511
name="${distribution}${tag//./}"

# init inventory
> tests/inventory

# If we have some custom actions to apply
if [ -f "tests/Dockerfile.${name}" ]; then
    docker build -t "$name" -f "tests/Dockerfile.${name}" tests/
    container_id="$name"
else
    container_id="$distribution:$tag"
fi

docker run --privileged=true -dt --name "$name" "$container_id" /sbin/init

# add to the inventory
echo "$name" >> tests/inventory

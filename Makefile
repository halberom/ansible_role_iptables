.PHONY: all
all: start_containers run1 run2 stop_containers

start_containers:
	tests/start_containers.sh

run1:
	tests/run_1.sh

run2:
	tests/run_2.sh

stop_containers:
	tests/stop_containers.sh

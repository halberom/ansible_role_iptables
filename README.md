# iptables

[![Build Status](https://travis-ci.org/halberom/ansible_role_iptables.svg?branch=master)](https://travis-ci.org/halberom/ansible_role_iptables)

Manage iptables by defining services to open up in a business function group, and allowed sources in an environment and/or location group.

## Notes
- primarily aimed at the redhat OS family, with some support for debian/ubuntu.
- currently only handles filter (input/output) and nat (prerouting).
- assumes only whitelisting will be performed.
- testing iptables service in docker requires starting containers with ```--privileged=true```

## Requirements

- Ansible requirements, e.g. Python 2.6 or 2.7 must be used as 3.x is not supported yet
- gather_facts (setup) needs to be run to provide the ```ansible_os_family``` fact for identifying which vars to use

## Role Variables

### Defaults

```
iptables_ipv4_manage: True
```
Whether or not to manage ipv4 rules

```
iptables_ipv6_manage: False
```
Whether or not to manage ipv6 rules

```
iptables_ipv4_input_filters:
  ssh:
    - proto: tcp
      from_port: 22
      to_port: 22
  icmp:
    - proto: icmp
      # icmp type
      from_port: -1
      # icmp code
      to_port: -1
```
A hash of service to proto/ports to grant access to

```
iptables_ipv4_input_filter_sources:
  ssh:
    - 192.168.0.1
    - 10.0.0.0/24
  icmp:
    - 0.0.0.0/0
```
A hash of service to cidr list, denoting who can access the service

### OS Family specific

See vars/ files for specifics

## Example Playbook

The following overrides the default services, and sets a non-standard ssh port

```
- hosts: servers
  vars:
    iptables_ipv4_input_filters:
      ssh_nonstd:
        - proto: tcp
          from_port: 31234
          to_port: 31234
  roles:
     - { role: halberom.iptables }
```

## Testing

This repo uses [Travis CI](https://travis-ci.org/) and docker containers to verify changes.  However you can also test locally for added speed.

_Make sure you have docker installed locally before continuing_

```
export distribution=centos
export tag=7.2.1511

tests/start_container.sh
tests/run_1.sh
tests/run_2.sh
tests/stop_containers.sh
```
or simply
```
make
```

## Why this repo

Some articles online describe using ferm and having each role add their own rules. Personally I don't like that approach, as in my opinion a role that installs e.g. nginx should not give a flying toss about iptables/ufw/firewalld etc.  Recently ansible added an iptables module, which is akin to running ```iptables -A INPUT ...``` in a shell command, but I see limited benefit to that vs using a template, as if I want it to survive reboots I need to store and restore regardless.  Using a template gives other advantages such as only applying what is defined, individual rule management necessitates ensuring objects are 'absent'.


## License

MIT

## Author Information

Gerard Lynch <gerard@halberom.co.uk>


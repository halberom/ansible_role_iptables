#!/bin/bash
set -x

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

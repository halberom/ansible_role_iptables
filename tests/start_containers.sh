#!/bin/bash
set -x

role_dir=$(dirname $(pwd))
role_name=$(basename $role_dir)

# Look for a list (string) of space separated containers, default to 'centos:7.2.1511'
[ "$CONTAINERS" ] || CONTAINERS='centos:7.2.1511'

# init inventory
> tests/inventory

for container in $CONTAINERS; do
    IFS=':' read -ra container_tag <<< "$container"
    distribution=${container_tag[0]}
    tag=${container_tag[1]}
    # use centos721511 as container name 
    name="${distribution}${tag//./}"
    unset IFS

    # If we have some custom actions to apply
    if [ -f "tests/Dockerfile.${name}" ]; then
        docker build -t $name "tests/Dockerfile.${name}"
        container_id=$name
    else
        container_id=$distribution:$tag
    fi

    # NOTE: docker containers for centos versions below 6.8 do not include
    # upstart so /sbin/init is not available
    docker run --privileged=true -dt --name $name $container_id /sbin/init

    # add to the inventory
    echo $name >> tests/inventory
done
